package com.gitee.l0km.javadocreader;

import java.io.File;
import java.lang.reflect.Method;
import java.util.List;

import static com.gitee.l0km.com4j.base.SimpleLog.log;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.gitee.l0km.javadocreader.example.ArrayUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JavadocReaderTest {
	/**
	 * 返回JVM属性 'java.class.path'定义的classpath，并加上当前项目的编译目标文件夹'target/classes'
	 */
	private static String  classpath() {
		StringBuffer buffer = new StringBuffer(System.getProperty("java.class.path"));
		buffer.append(File.pathSeparatorChar).append(new File("target/classes").getAbsolutePath());
		return buffer.toString();
	}
	@Test
	public void test1() {
		try {
			JavadocReader.setSourcepath(new File("src/test/java").getAbsolutePath());
			JavadocReader.setClasspath(classpath());
			ExtClassDoc classDoc = JavadocReader.read(ArrayUtils.class);
			assertNotNull(classDoc);
			String classComment = classDoc.getClassComment();
			log("Comment:{}", classComment);
			assertFalse(classComment.isEmpty());
			assertTrue(classComment.indexOf("清除array中为{@code null}的元素") > 0);
			assertTrue(classComment.indexOf("@author Pascal.A.Join") > 0);
			assertTrue(classComment.indexOf("@since 2.0.0") > 0);
			
			for(Method m:ArrayUtils.class.getDeclaredMethods()) {
				log("{}",m);
			}
			{
				Method method = ArrayUtils.class.getMethod("cleanNull", Object[].class);
				log("{}",method);
				String methodComment = classDoc.getMethodComment(method);
				log("{}",methodComment);
				assertNotNull(methodComment);
				assertTrue(methodComment.indexOf("Clear the elements in the array that are {@ code null}") > 0);
				assertTrue(methodComment.indexOf("@param <T> component type of array") > 0);
				{
					String argComment = classDoc.parameterCommentOf(method, "array");
					log("arg comment:{}",argComment);
					assertTrue(argComment.indexOf("输入的数组") >= 0);
				}
				{
					String argComment = classDoc.parameterCommentOf(method, "<T>");
					log("arg comment:{}",argComment);
					assertTrue(argComment.indexOf("component type") >= 0);
				}
			}
			{
				Method method = ArrayUtils.class.getMethod("indexOfFirstNull", Object[].class);
				log("{}",method);
				String methodComment = classDoc.getMethodComment(method);
				log("{}",methodComment);
				assertNotNull(methodComment);
				assertTrue(methodComment.indexOf("返回第一个为{@code null}元素的索引,没找到返回-1,如果输入为{@code null}则返回-2") > 0);
				assertTrue(methodComment.indexOf("@param objects") > 0);
				{
					String argComment = classDoc.parameterCommentOf(method, "objects");
					log("arg comment:{}",argComment);
					assertTrue(argComment.isEmpty());
				}
			}
			{
				Method method = ArrayUtils.class.getMethod("testPrimitiveArg", int.class);
				log("{}",method);
				String methodComment = classDoc.getMethodComment(method);
				log("{}",methodComment);
				assertNotNull(methodComment);
				assertTrue(methodComment.indexOf("test primitive argument") > 0);
				assertTrue(methodComment.indexOf("@param a1 input integer") > 0);
				{
					String argComment = classDoc.parameterCommentOf(method, "a1");
					log("arg comment:{}",argComment);
					assertTrue(argComment.equals("input integer"));
				}
			}
			{
				Method method = ArrayUtils.class.getMethod("addUser", List.class);
				log("{}",method);
				String methodComment = classDoc.getMethodComment(method);
				log("{}",methodComment);
				assertNotNull(methodComment);
				assertTrue(methodComment.indexOf("add users(添加用户)") > 0);
				assertTrue(methodComment.indexOf("@param users user list") > 0);
				String commentText = classDoc.commentTextOf(method);
				log("commentText\n[{}]",commentText);
			}
			{
				String fieldComment = classDoc.getFieldComment("ISO8601_FORMATTER_STR",false,false, true);
				log("[{}]",fieldComment);
				assertTrue(fieldComment.indexOf("ISO8601时间格式")>=0);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void test2() {
		try {
			JavadocReader.setSourcepath(new File("src/test/java").getAbsolutePath());
			JavadocReader.setClasspath(classpath());
			ExtClassDoc classDoc = JavadocReader.read(ArrayUtils.UserInfo.class);
			assertNotNull(classDoc);
			String classComment = classDoc.getClassComment();
			log("Comment:{}", classComment);
			assertFalse(classComment.isEmpty());
			assertTrue(classComment.indexOf("user info") > 0);
			assertTrue(classComment.indexOf("@author guyadong") > 0);
			
			{
				Method method = ArrayUtils.UserInfo.class.getMethod("addConnector", List.class);
				log("{}",method);
				String methodComment = classDoc.getMethodComment(method);
				log("{}",methodComment);
				assertNotNull(methodComment);
			}
			{
				Method method = ArrayUtils.UserInfo.class.getMethod("addAddress", String.class);
				log("{}",method);
				String methodComment = classDoc.getMethodComment(method);
				log("{}",methodComment);
				assertNotNull(methodComment);
				assertTrue(methodComment.indexOf("add address") > 0);
			}
			{
				String fieldComment = classDoc.getFieldComment("name",false,false, true);
				log("[{}]",fieldComment);
				assertTrue(fieldComment.indexOf("用户名")>=0);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			fail();
		}
	}

}
