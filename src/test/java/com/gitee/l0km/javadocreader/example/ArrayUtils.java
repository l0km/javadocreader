package com.gitee.l0km.javadocreader.example;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 清除array中为{@code null}的元素
 * @author Pascal.A.Join
 * @since 2.0.0
 *
 */
public class ArrayUtils {

	/** ISO8601时间格式 */
    public static final String ISO8601_FORMATTER_STR = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	private ArrayUtils() {}
	/**
	 * Clear the elements in the array that are {@ code null}
	 * @param <T> component type of array
	 * @param array 输入的数组
	 * @return 如果 array为{@code null}或长度为0或array中没有{@code null}元素则返回array,否则返回新的不含{@code null}的数组
	 * @throws IllegalArgumentException invalid argument
	 */
	public static final <T> T[] cleanNull(T[] array){
		 return null;
	}
    /**
     * 返回第一个为{@code null}元素的索引,没找到返回-1,如果输入为{@code null}则返回-2
     * @param objects
     */
    public static int indexOfFirstNull(Object...objects) {
		return -2;
	}
	/**
	 * @param objects 
	 * @return true if any one of object is null or objects is null 
	 */
	public static boolean hasNull(Object...objects) {
		return indexOfFirstNull(objects) != -1;
	}
	/**
	 * <p>Adds all the elements of the given arrays into a new array.
	 * <p>The new array contains all of the element of {@code array1} followed
	 * by all of the elements {@code array2}. When an array is returned, it is always
	 * a new array.
	 *
	 * <pre>
	 * ArrayUtils.addAll(null, null)     = null
	 * ArrayUtils.addAll(array1, null)   = cloned copy of array1
	 * ArrayUtils.addAll(null, array2)   = cloned copy of array2
	 * ArrayUtils.addAll([], [])         = []
	 * ArrayUtils.addAll([null], [null]) = [null, null]
	 * ArrayUtils.addAll(["a", "b", "c"], ["1", "2", "3"]) = ["a", "b", "c", "1", "2", "3"]
	 * </pre>
	 *
	 * @param <T> the component type of the array
	 * @param array1  the first array whose elements are added to the new array, may be {@code null}
	 * @param array2  the second array whose elements are added to the new array, may be {@code null}
	 * @return The new array, {@code null} if both arrays are {@code null}.
	 *      The type of the new array is the type of the first array,
	 *      unless the first array is null, in which case the type is the same as the second array.
	 * @since 2.1
	 * @throws IllegalArgumentException if the array types are incompatible
	 */
	public static <T> T[] addAll(final T[] array1, final T... array2) {
	    return null;
	}
	// Clone
	//-----------------------------------------------------------------------
	/**
	 * <p>Shallow clones an array returning a typecast result and handling
	 * {@code null}.
	 *
	 * <p>The objects in the array are not cloned, thus there is no special
	 * handling for multi-dimensional arrays.
	 *
	 * <p>This method returns {@code null} for a {@code null} 输入的数组.
	 *
	 * @param <T> the component type of the array
	 * @param array  the array to shallow clone, may be {@code null}
	 * @return the cloned array, {@code null} if {@code null} input
	 */
	public static <T> T[] clone(final T[] array) {
	    return null;
	}
	/**
	 * test primitive argument
	 * @param a1 input integer
	 * @return always 0
	 */
	public static int testPrimitiveArg(int a1) {
		return 0;
	}
	/**
	 * add users(添加用户)
	 * @param users user list
	 * @throws IllegalArgumentException invalid argument
	 */
	public static void addUser(List<UserInfo> users) {
	}
	public static class BaseUserInfo{
		/**
		 * 用户名
		 */
		protected String name;
		/**
		 * add address
		 * @param address address string
		 */
		public void addAddress(String address) {}
	}
	/**
	 * user info
	 * @author guyadong
	 *
	 */
	public static class UserInfo extends BaseUserInfo {
		/**
		 * return name 
		 * @return user name 
		 */
		public String getName() {
			return null;
		}
		@Override
		public void addAddress(String address) {}
		/**
		 * add contacts
		 * @param users
		 * @throws IOException TODO
		 * @throws IllegalArgumentException TODO
		 */
		public void addConnector(List<UserInfo> users) throws IOException, IllegalArgumentException {}
		/**
		 * write user information to file
		 * @param to
		 */
		public void outputFile(File to) {}
	}
}
