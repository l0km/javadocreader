package com.gitee.l0km.javadocreader;

import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;
import org.junit.Test;

import com.gitee.l0km.com4j.base.SimpleLog;

public class RegexTest {
	public static final String NEW_LINE = System.getProperty("line.separator");
	private static final String commentBody = "/**" + NEW_LINE + " */" + NEW_LINE;
	@Test
	public void testReplaceFirst() {
		try {
			String comment = "$(hello)";
			SimpleLog.log("{}",Matcher.quoteReplacement(comment));
			String cmt = commentBody.replaceFirst(NEW_LINE, "$0" + Matcher.quoteReplacement(comment));
			SimpleLog.log("{}",cmt);			
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

}
