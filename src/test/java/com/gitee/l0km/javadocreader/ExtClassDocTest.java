package com.gitee.l0km.javadocreader;

import static com.gitee.l0km.com4j.base.SimpleLog.log;
import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.gitee.l0km.javadocreader.ExtClassDoc;
import com.gitee.l0km.javadocreader.JavadocReader;
import com.sun.javadoc.MemberDoc;

@SuppressWarnings("restriction")
public class ExtClassDocTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		JavadocReader.setSourcepath("./src/test/java");
	}

	@Test
	public void test1() {
		try {
			{
				Method method = App.class.getMethod("test0", UserBean.class,List.class,List.class,Integer.class,int[].class,UserBean[][].class);
//			Method method = App.class.getMethod("test0", BaseUserBean.class,List.class,List.class,Integer.class,int[].class,BaseUserBean[].class);
				log("method:{}",method);
				ExtClassDoc extClassDoc= JavadocReader.read(App.class);
				assertNotNull(extClassDoc);
				MemberDoc memberDoc = extClassDoc.getMethodDoc(method);
				assertNotNull(memberDoc);
				log("{}",memberDoc.getRawCommentText());
				assertFalse(memberDoc.getRawCommentText().isEmpty());
			}
			{
				Method method = App.class.getMethod("test1", BaseUserBean.class,List.class,List.class,Integer.class,int[].class,BaseUserBean[].class);
				log("method:{}",method);
				ExtClassDoc extClassDoc= JavadocReader.read(App.class);
				assertNotNull(extClassDoc);
				MemberDoc memberDoc = extClassDoc.getMethodDoc(method);
				log("{}",memberDoc.getRawCommentText());
				assertFalse(memberDoc.getRawCommentText().isEmpty());
			}
			{
				Method method = App.class.getMethod("test3", Date[].class,Date[][].class,Map.class);
				log("method:{}",method);
				ExtClassDoc extClassDoc= JavadocReader.read(App.class);
				assertNotNull(extClassDoc);
				MemberDoc memberDoc = extClassDoc.getMethodDoc(method);
				log("{}",memberDoc.getRawCommentText());
				assertFalse(memberDoc.getRawCommentText().isEmpty());
			}
		} catch (Throwable e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void test2() {
		try {
			for(Method method:App.class.getMethods()) {
				log("{}",method);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			fail();
		}
	}

	public static final String getTypeName(Class<?> type) {
		StringBuilder dimensions = new StringBuilder();
		while (type.isArray()) {
			dimensions.append("[]");
			type = type.getComponentType();
		} 
		String name = type.getName().replaceAll("\\$", "\\.");
		return new StringBuilder(name).append(dimensions).toString();
	}
	public static class  BaseApp<USER_T extends BaseUserBean>{
		
		/**
		 * hello,test0
		 * @param user user
		 * @param users users
		 * @param ids ids
		 * @param groupId group id 
		 * @param idarray id array
		 * @param userArray user array
		 */
		public void test0(USER_T user,List<USER_T>users,List<Integer>ids,Integer groupId,int[] idarray,USER_T[][] userArray) {}
		/**
		 * hello,test1
		 * @param user user
		 * @param users users
		 * @param ids ids
		 * @param groupId group id 
		 * @param idarray id array
		 * @param userArray user array
		 */
		public void test1(USER_T user,List<USER_T>users,List<Integer>ids,Integer groupId,int[] idarray,USER_T[]userArray) {}
		/**
		 * hello,test3
		 * @param idarray id array
		 * @param userArray user array
		 */
		public <T extends Date>void test3(T[] idarray,T[][]userArray,Map<String,T> map) {}
	}
	public static class  App extends BaseApp<UserBean>{

		@Override
		public void test0(UserBean user, List<UserBean> users, List<Integer> ids, Integer groupId, int[] idarray,UserBean[][]userArray) {
			super.test0(user, users, ids, groupId, idarray, userArray);
		}
		
	}
	public static class BaseUserBean{}
	public static class UserBean extends BaseUserBean{}
}
