package com.gitee.l0km.javadocreader;

import com.gitee.l0km.com4j.basex.doc.ClassCommentProvider;
import com.gitee.l0km.com4j.basex.doc.ClassCommentProviderFactory;

public class ClassCommentProviderFactoryImpl implements ClassCommentProviderFactory {

	public ClassCommentProviderFactoryImpl() {
	}

	@Override
	public ClassCommentProvider apply(Class<?> clazz) {
		return new ClassCommentProviderImpl(clazz);
	}

}
